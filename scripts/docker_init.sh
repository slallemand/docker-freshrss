#!/bin/sh

mkdir -p "$DOCKER_CERT_PATH"
echo "$CA" > $DOCKER_CERT_PATH/ca.pem
echo "$CLIENT_CERT" > $DOCKER_CERT_PATH/cert.pem
echo "$CLIENT_KEY" > $DOCKER_CERT_PATH/key.pem

docker login -u ${DOCKER_USER} -p ${DOCKER_PASSWORD}