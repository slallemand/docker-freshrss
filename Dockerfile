FROM alpine:latest AS builder
LABEL maintainer seb <dev@lallemand.fr>

WORKDIR /var/www/

RUN apk add --update git && \
    git clone https://github.com/FreshRSS/FreshRSS.git



FROM alpine:latest AS engine
LABEL maintainer seb <dev@lallemand.fr>

ENV UID=1337 \
    FRESHRSS_ROOT=/var/www/FreshRSS

COPY --from=builder /var/www/FreshRSS /var/www/FreshRSS

RUN apk add --no-cache \
    apache2 php7-apache2 \
    php7 php7-curl php7-gmp php7-intl php7-mbstring php7-xml php7-zip \
    php7-ctype php7-dom php7-fileinfo php7-iconv php7-json php7-session php7-simplexml php7-xmlreader php7-zlib \
    php7-pdo_sqlite \
    php7-pdo_mysql \
    php7-pdo_pgsql \
    su-exec

RUN mkdir -p ${FRESHRSS_ROOT} /run/apache2/
WORKDIR ${FRESHRSS_ROOT}

RUN php -f ./cli/prepare.php > /dev/null && \
    chown -R :www-data ${FRESHRSS_ROOT} && \
    chmod -R g+r ${FRESHRSS_ROOT} && \
    chmod -R g+w ${FRESHRSS_ROOT}/data/

## Running without root
RUN sed 's/^Listen .*:80$/Listen 0.0.0.0:8080/' ./Docker/FreshRSS.Apache.conf > /etc/apache2/conf.d/FreshRSS.Apache.conf && \
    sed -i -e 's/^Listen 80$/#Listen 8080/' /etc/apache2/httpd.conf && \
    ln -s /dev/stdout /var/www/logs/access.log && \
    ln -s /dev/stderr /var/www/logs/error.log && \
    chown -R $UID /run/apache2/ && \
    chown -R $UID ${FRESHRSS_ROOT} && \
    chown -R $UID /var/log/apache2

EXPOSE 8080
USER $UID

CMD exec httpd -D FOREGROUND
